import argparse
from tensorboardX import SummaryWriter
import random
import yaml
from itertools import chain
import logging
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
from datetime import datetime
import torch
from torch import nn
from torch import optim
from itertools import cycle

from .model import Encoder, Decoder
from .data import toy_prepost
from .grammars.pre_ten import language as pre_language
from .grammars.post_ten import language as post_language


def metrics(predict, target):
    string = ""
    target = target.squeeze(dim=0).tolist()
    string += "\n    Target:  " + str(pre_language.gen.generate(target, render=True))
    beam = torch.argmax(predict, dim=1).squeeze(dim=0).tolist()
    string += "\n    Predic: " + str(post_language.gen.generate(beam, render=True))
    matches = [
        i == j for i, j in zip(target, (beam + [None] * len(target))[: len(target)])
    ]
    em = sum(matches) / len(matches)
    return {"em": em, "string": string}


class StandaloneCorpus(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        string, derivation = self.data[index]
        seq = torch.Tensor(derivation).long()
        return {"string": string, "derivation": derivation, "seq": seq}


class ParallelCorpus(Dataset):
    def __init__(self, data):
        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        pre_s, pre_d, post_s, post_d = self.data[index]
        pre_seq = torch.Tensor(pre_d).long()
        post_seq = torch.Tensor(post_d).long()
        return {
            "pre_s": pre_s,
            "pre_d": pre_d,
            "post_s": post_s,
            "post_d": post_d,
            "pre_seq": pre_seq,
            "post_seq": post_seq,
        }


writer = SummaryWriter(f"logs/{datetime.utcnow()}")
logger = logging.getLogger(__name__)
parser = argparse.ArgumentParser()
parser.add_argument("--hdim", default=32, type=int)
parser.add_argument("--depth", default=5, type=int)
parser.add_argument("--n_per_corpus", default=1000, type=int)
parser.add_argument("--accumulate_step", default=10, type=int)
parser.add_argument("--log_freq", default=3, type=int)
parser.add_argument("--train_control", default="train_control.yml", type=str)
parser.add_argument("--control_refresh_freq", default=10, type=int)
args = parser.parse_args()


# models
pre_enc = Encoder(args.hdim, pre_language.gen, args.depth)
post_enc = Encoder(args.hdim, post_language.gen, args.depth)
pre_dec = Decoder(args.hdim, pre_language.gen, args.depth)
post_dec = Decoder(args.hdim, post_language.gen, args.depth)
# dataset
pre, post, parallel = toy_prepost(n_per_corpus=args.n_per_corpus)
pre = StandaloneCorpus(pre)
post = StandaloneCorpus(post)
paratest, parallel = (
    ParallelCorpus(parallel[-args.accumulate_step :]),
    ParallelCorpus(parallel[500:]),
)
# train

criterion = nn.CrossEntropyLoss(reduction="sum")
optimizer = optim.Adam(
    list(
        chain(
            pre_enc.parameters(),
            post_enc.parameters(),
            pre_dec.parameters(),
            post_dec.parameters(),
        )
    )
)


def check_train_control():
    with open(args.train_control, "r") as fl:
        cfg = yaml.load(fl.read(), Loader=yaml.BaseLoader)
    return cfg


def single_stepper(cfg):
    ...


def mixed_stepper(cfg):
    ...


def parallel_stepper(cfg):
    global steps
    if not hasattr(parallel, "ordering") or len(parallel.ordering) == 0:
        parallel.ordering = list(range(len(parallel)))
        random.shuffle(parallel.ordering)
    # ----------
    sample = parallel[parallel.ordering.pop()]
    x = sample["pre_seq"].unsqueeze(dim=0)
    y = sample["post_seq"].unsqueeze(dim=0)
    y_ = pre_enc(x)
    y_ = post_dec(y_)
    loss = criterion(y_[:, :, : y.shape[1]], y)
    if loss <= 0.5:  # successful-ish conversion to post
        x_ = post_enc(y_, soft=True)
        x_ = pre_dec(x_)
        loss = loss + criterion(x_[:, :, : x.shape[1]], x)
    loss.backward()
    return loss


fn_map = {
    "parallel": parallel_stepper,
    "single": single_stepper,
    "mixed": mixed_stepper,
}

steps = 0
cfg = check_train_control()
loss = []
for _ in tqdm(cycle([None])):
    steps += 1
    if steps % args.control_refresh_freq == 0:
        cfg = check_train_control()
    if steps % args.accumulate_step == 0:
        optimizer.step()
        optimizer.zero_grad()
    step_fn = fn_map[cfg["fn"]]
    loss.append(step_fn(cfg).detach().item())
    if steps % args.log_freq == 0:
        writer.add_histogram("train/loss_dist", loss, steps)
        writer.add_scalar("train/loss", sum(loss) / len(loss), steps)
        loss = []
