import torch
from torch import nn


class FixupBlock(nn.Module):
    def __init__(self, hdim):
        super().__init__()
        kernel = 3
        self.bias1a = nn.Parameter(torch.zeros(1))
        self.conv1 = nn.Conv1d(hdim, hdim, kernel_size=kernel, padding=1)
        self.bias1b = nn.Parameter(torch.zeros(1))
        self.relu = nn.ReLU(inplace=True)
        self.bias2a = nn.Parameter(torch.zeros(1))
        self.conv2 = nn.Conv1d(hdim, hdim, kernel_size=kernel, padding=1)
        self.bias2b = nn.Parameter(torch.zeros(1))
        self.scale = nn.Parameter(torch.ones(1))

    def forward(self, x):
        identity = x
        out = self.conv1(x + self.bias1a)
        out = self.relu(out + self.bias1b)
        out = self.conv2(out + self.bias2a)
        out = out * self.scale + self.bias2b
        out += identity
        out = self.relu(out)
        return out


class Encoder(nn.Module):
    def __init__(self, hdim, gen, depth, kernel=3):
        super().__init__()
        self.hdim = hdim
        self.gen = gen
        # -----------------
        self.rules = nn.Embedding(self.gen.n_rules, hdim, padding_idx=0)
        self.process = nn.Sequential(*[FixupBlock(hdim) for _ in range(depth)])
        # =========== init / consts
        torch.nn.init.normal_(self.rules.weight, mean=0.0, std=0.1)

    def forward(self, x, *, soft=False):
        if not soft:
            x = self.rules(x.squeeze(dim=0).long()).permute(1, 0).unsqueeze(dim=0)
        else:
            x = x.squeeze(dim=0).permute(1, 0)  # 1RL -> RL -> LR
            x = nn.functional.softmax(x, dim=1)  # LR -> LR
            all_rules = (
                torch.Tensor(range(self.gen.n_rules))
                .unsqueeze(dim=0)
                .expand(x.shape[0], -1)
                .long()
            )  # LR
            all_rules = self.rules(all_rules)  # LR -> LRH
            x = x.unsqueeze(dim=2)  # LR -> LR1
            x = (x * all_rules).sum(dim=1)  # LR1*LRH -> LRH -> LH
            x = x.permute(1, 0).unsqueeze(dim=0)  # LH -> HL -> 1HL
        x = self.process(x)  # 1HL -> 1HL
        return x.sum(dim=2)  # 1HL -> 1H


class Decoder(nn.Module):
    def __init__(self, hdim, gen, depth, kernel=3, maxl=20):
        super().__init__()
        self.hdim = hdim
        self.gen = gen
        self.maxl = maxl
        # -----------------
        self.rules = nn.Embedding(self.gen.n_rules, hdim, padding_idx=0)
        self.process = nn.Sequential(*[FixupBlock(hdim) for _ in range(depth)])
        # =========== init / consts
        torch.nn.init.normal_(self.rules.weight, mean=0.0, std=0.1)

    def forward(self, x):
        x = x.permute(1, 0)  # 1H -> H1
        output = []
        derivation = []
        for step in range(self.maxl):
            vidx = self.gen.valid_rules(derivation)  # list of valid indices
            valid = self.rules(torch.Tensor(vidx).long()).T.unsqueeze(dim=0)
            # KH -> HK -> 1HK
            valid = self.process(valid)
            valid = valid.squeeze(dim=0).permute(1, 0)  # 1HK ->HK -> KH
            choice = valid @ x  # KH @ H1 -> K1
            p = torch.zeros(self.gen.n_rules)
            assert len(vidx) == choice.shape[0]
            for idx, val in zip(vidx, choice.squeeze(dim=1)):
                p[idx] = val
            output.append(p)
            try:
                derivation.append(vidx[torch.argmax(p)])
            except IndexError:
                print(derivation)
                print(output)
                raise
        return torch.stack(output, dim=1).unsqueeze(dim=0)  # NR -> 1NR
