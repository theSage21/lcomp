import random
from .grammars.pre_ten import language as pre_ten
from .grammars.post_ten import language as post_ten
from tqdm import tqdm


class Op:
    def __init__(self, op, left, right):
        self.op = op
        self.left = left
        self.right = right

    def pre(self):
        return f"{self.op} {self.left.pre()} {self.right.pre()}"

    def post(self):
        return f"{self.left.post()} {self.right.post()} {self.op}"


class Num:
    def __init__(self, num):
        self.num = num

    def pre(self):
        return f"{self.num}"

    def post(self):
        return f"{self.num}"


def new_eqn(depth=0, limit=3):
    if depth >= limit or random.choice(range(100)) > 80:
        return Num(random.choice("0123456789"))
    else:
        return Op(
            random.choice("-+"), new_eqn(depth=depth + 1), new_eqn(depth=depth + 1)
        )


def toy_prepost(n_per_corpus=10_000, maxl=200):
    pre_data = set()
    post_data = set()
    parallel = set()
    with tqdm(total=n_per_corpus, desc="Pre") as pbar:
        while len(pre_data) < n_per_corpus:
            string = new_eqn().pre()
            derivation = tuple(pre_ten.parser.parse(pre_ten.lexer.tokenize(string)))
            pair = (string, derivation)
            if pair not in pre_data:
                pre_data.add(pair)
                pbar.update(1)
    with tqdm(total=n_per_corpus, desc="Post") as pbar:
        while len(post_data) < n_per_corpus:
            string = new_eqn().post()
            derivation = tuple(post_ten.parser.parse(post_ten.lexer.tokenize(string)))
            pair = (string, derivation)
            if pair not in post_data:
                post_data.add(pair)
                pbar.update(1)
    with tqdm(total=n_per_corpus, desc="Parallel") as pbar:
        while len(parallel) < n_per_corpus:
            eqn = new_eqn()
            pre_string = eqn.pre()
            post_string = eqn.post()
            post_derivation = tuple(
                post_ten.parser.parse(post_ten.lexer.tokenize(post_string))
            )
            pre_derivation = tuple(
                pre_ten.parser.parse(pre_ten.lexer.tokenize(pre_string))
            )
            pair = (pre_string, pre_derivation, post_string, post_derivation)
            if pair not in parallel:
                parallel.add(pair)
                pbar.update(1)
    return list(pre_data), list(post_data), list(parallel)
