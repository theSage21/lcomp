from sly import Lexer, Parser
from typing import Dict, List, Tuple
from collections import defaultdict
from itertools import chain
from collections import namedtuple


language = namedtuple("language", "lexer parser gen")


def expand_rhs(rhs):
    string = ""
    for index, part in enumerate(rhs.split()):
        if part.islower():
            string += f"+ p[{index}]"
    return string


class Generator:
    def __init__(self):
        self.rules = {0: None}  # PAD
        self.n_rules = 1
        self.start_symbol = None
        self.renders = dict()

    def new_rule(self, lhs, rhs):
        if self.n_rules in self.rules:
            raise Exception("Cannot reassign index")
        self.rules[self.n_rules] = (lhs, rhs.split())
        if self.n_rules == 1:  # 0 is No-OP
            self.start_symbol = lhs
        self.n_rules += 1

    def generate(self, derivation, render=False):
        string = [self.start_symbol]
        for i in (i for i in derivation if i > 0):
            lhs, rhs = self.rules[i]
            if lhs in string:
                i = string.index(lhs)
                string = string[:i] + rhs + string[i + 1 :]
        return (
            string if not render else " ".join(self.renders.get(i, i) for i in string)
        )

    def valid_rules(self, derivation):
        # TODO: make more efficient
        derivation = [] if derivation is None else derivation
        string = self.generate(derivation)
        valid = [
            idx for idx, _ in self.rules.items() if _ is not None and _[0] in string
        ]
        return valid + [0]  # pad is always valid

    def add_render(self, symbol, string):
        self.renders[symbol] = string


def build(
    tokens: Dict[str, str] = None,
    render: Dict[str, str] = None,
    substitutions: List[Tuple[str]] = [],
    rules: List = None,
    ignore=" \t",
    verbose=False,
):
    token_list = (
        "{"
        + ",".join(str(i) for i in chain(tokens, (tok for _, _, tok in substitutions)))
        + "}"
    )
    regexes = "\n    ".join(f"{key} = r'{val}'" for key, val in tokens.items())
    subs = "\n    ".join(
        f"{parent}['{child}'] = {symbol}" for child, parent, symbol in substitutions
    )
    functions = "\n".join(
        line
        for index, (lhs, rhs) in enumerate(rules, start=1)
        for line in [
            f"    @_('{rhs}')",
            f"    def {lhs}(self, p):",
            f"        return [{index}]{expand_rhs(rhs)}",
        ]
    ).strip()

    string = fr"""class MyLexer(Lexer):
    tokens = {token_list}
    ignore = "{ignore}"
    
    {regexes}
    {subs}


class MyParser(Parser):
    tokens = MyLexer.tokens

    {functions}
    """
    if verbose:
        print(string)
    ctx = {"Lexer": Lexer, "Parser": Parser}
    exec(string, ctx)
    gen = Generator()
    for _, (lhs, rhs) in enumerate(rules):
        gen.new_rule(lhs, rhs)
    gen.renders.update(render)
    return language(ctx["MyLexer"](), ctx["MyParser"](), gen)
