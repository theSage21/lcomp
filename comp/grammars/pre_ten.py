from .core import build

language = build(
    tokens=dict(
        ONE="1",
        TWO="2",
        THREE="3",
        FOUR="4",
        FIVE="5",
        SIX="6",
        SEVEN="7",
        EIGHT="8",
        NINE="9",
        ZERO="0",
        ADD=r"\+",
        SUB=r"\-",
    ),
    render=dict(
        ONE="1",
        TWO="2",
        THREE="3",
        FOUR="4",
        FIVE="5",
        SIX="6",
        SEVEN="7",
        EIGHT="8",
        NINE="9",
        ZERO="0",
        ADD="+",
        SUB="-",
    ),
    rules=[
        ("exp", "ADD exp exp"),
        ("exp", "SUB exp exp"),
        ("exp", "ZERO"),
        ("exp", "ONE"),
        ("exp", "TWO"),
        ("exp", "THREE"),
        ("exp", "FOUR"),
        ("exp", "FIVE"),
        ("exp", "SIX"),
        ("exp", "SEVEN"),
        ("exp", "EIGHT"),
        ("exp", "NINE"),
    ],
)
