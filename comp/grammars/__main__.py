from .pre_ten import language as pre_ten
from .post_ten import language as post_ten


input = " - + 1 2 9"
for tok in list(pre_ten.lexer.tokenize(input)):
    print(tok)
# ------------------
input = " 1 2 9 +-"
for tok in list(post_ten.lexer.tokenize(input)):
    print(tok)
# ------------------
